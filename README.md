# Retailer Selection

This Tampermonkey userscript will:
1. Create a _Select None_ button to unmark all retailers
2. Make the search bar bigger and always visible
3. Fix the bug where if you select a retailer while the search filter is on, the list will scroll down all the way

## Getting started

Before starting, you need to have [Tampermonkey](https://chromewebstore.google.com/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) installed on your browser.

1. Go to [retailer_selection.js](https://gitlab.com/c.debarros/retailer_selection/-/blob/main/retailer_selection.js)
2. Copy the file content

![Copy File Contents](copy_file_contents.png)

3. Add a new script to your Tampermonkey extension

![Create New Script](create_new_script.png)

4. Paste the content to the editor and save

![Save the script](save_script.png)

You should see the new script in the _Installed Userscripts_ list. Enjoy!