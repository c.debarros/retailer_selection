// ==UserScript==
// @name         Retailer Selection
// @namespace    http://tampermonkey.net/
// @version      2024-04-29
// @downloadURL  https://gitlab.com/c.debarros/retailer_selection/-/raw/main/retailer_selection.js
// @updateURL    https://gitlab.com/c.debarros/retailer_selection/-/raw/main/retailer_selection.js
// @description  Make searching retailers easier
// @author       You
// @match        https://retailmedia.criteo.com/*
// @match        https://commercemax.criteo.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=criteo.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const selectNone = () => {
    document.querySelectorAll('.mat-select-panel-wrap mat-pseudo-checkbox')
        .forEach(checkbox => {
            if (checkbox.className.match('checked')) {
                checkbox.click()
            }
        })
    };

    const searchStyles = () => {
        const searchContainer = document.createElement('div');
    searchContainer.id = 'search-container';
    searchContainer.style.cssText = `
        display: flex;
        gap: 5px;
        padding: 10px;
        z-index: 2;
        min-width: calc(100% + 64px);
        background-color: white;
        margin-top: 32px;
    `;
    document.querySelector('.mat-select-panel-wrap').prepend(searchContainer);
    const clearButton = document.createElement('button');
    clearButton.style.fontSize = '1.2rem';
    clearButton.style.minWidth = '90px';
    clearButton.className = 'mat-focus-indicator mat-flat-button mat-button-base mat-primary';
    const clearButtonTxt = document.createElement('span');
    clearButtonTxt.className = 'mat-button-wrapper';
    clearButtonTxt.innerText = 'Select None';
    const clearButtonRipple = document.createElement('span');
    clearButtonRipple.className = 'mat-ripple mat-button-ripple'
    const clearButtonFocus = document.createElement('span');
    clearButtonFocus.className = 'mat-button-focus-overlay';
    clearButton.appendChild(clearButtonTxt);
    clearButton.appendChild(clearButtonRipple);
    clearButton.appendChild(clearButtonFocus);
    clearButton.addEventListener('click', selectNone);
    const searchField = document.querySelector('.mat-select-panel-wrap input');
    searchField.style.cssText = `
        background-color: white;
        min-width: 140px;
        border-width: 1px;
        border-color: lightgray;
        border-style: solid;
    `;
    searchContainer.prepend(searchField);
    searchContainer.prepend(clearButton);
    document.querySelector('.mat-select-panel-wrap [role=listbox]').style.margin = 0;
    }

    let listboxScroll = 0;

    const retailerToolsSearch = (records, observer) => {
        if(window.location.pathname.match('retailerTools')){
            for(let i = 0; i < records.length; i++){
                // console.log('mutation!!');
                for (let j = 0; j < records[i].addedNodes.length; j++) {
                    // console.log(records[i].addedNodes[j].className);
                    if(records[i].addedNodes[j].className?.match('cdk-overlay-backdrop cdk-overlay-transparent-backdrop')){
                        console.log('panel-wrap!!!');
                        const listbox = document.querySelector('div[role=listbox]');
                        listbox.addEventListener('scroll', ()=>{listboxScroll = listbox.scrollTop});
                        console.log(listboxScroll);
                        listbox.addEventListener('click', ()=>{listbox.scrollTop = listboxScroll});
                        searchStyles();
                    }
                }
            }
        }
    }

    const observerOptions = {
        childList: true,
        subtree: true,
    };
    const container = document.querySelector('body');
    const observer = new MutationObserver(retailerToolsSearch);
    observer.observe(container,observerOptions);
})();